close all;
clear all;
I_CQ=1*10^-3
V_CC=5
h_FE_min=330
V_BE=1.2
V_T= 25*10^-3

printf("Collector Q-point average current I: %f [A]\n", I_CQ);
printf("Voltage source: %i [V]\n",V_CC);
printf("BJT open loop minimum gain h_FE: %i\n",h_FE_min);
printf("BJT average active-forward V_BE drop: %f\n",V_BE);
I_CQ_max = I_CQ * 2;
R_CE_min=V_CC/I_CQ_max;

R_C = R_CE_min * (10/10);
R_E = R_CE_min * (1/10);
printf("Collector Resistor: %i [Ohm]\n",R_C);
printf("Emitter Resistance R_EH + R_EL : %i [Ohm]\n",R_E);

V_C_min =V_CC - R_C*I_CQ_max;
printf("Collector dynamic voltage range: [%f: %f] [V]\n",V_C_min, V_CC);

V_CQ=(V_C_min + V_CC)/2;
printf("Collector Q-point average voltage: %f [V]\n",V_CQ);

r_e=V_T/I_CQ;
printf("Intrinsec BJT emitter resistance r_e: %f [Ohm]\n",r_e);

r_IN_base_min=(R_E+r_e)*h_FE_min;
R_B2_max=r_IN_base_min/10;

printf("Maximum R_B2 resistance for voltage divider: %f [Ohm]\n",R_B2_max);

V_EQ=R_E*I_CQ;
V_BQ=V_BE+V_EQ;
I_B2=V_BQ/R_B2_max;
R_B1_max=(V_CC-V_BQ)/I_B2;
printf("Maximum R_B1 resistance for voltage divider: %f [Ohm]\n",R_B1_max);

printf("Voltage divider current: %f [A]\n",I_B2);








