#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/*
 *       ___ V_cc
 *        |
 *     ___|___
 *    |       |
 *    |       |
 *   R_B1    R_C
 *    |       |
 *    |       C
 *    |       |
 *    |-----B-Q
 *    |       |
 *    |       E
 *    |       |
 *   R_B2    R_E
 *    |       |
 *    |_______|
 *        |
 *      ---- Gnd
 */

//BJT specs
float Beta=120;
float V_BE=0.7;
float I_C_max=0.100;
float V_CE_sat=0.2;
float V_CE_max=25;

//Bias network specs
float V_cc=5;

//Min Voltage Divider (R_B1, R_B2) current to Base current I_B ration
float I_factor=10;


//Input Values
float I_CQ = 0.010; 	//Bias collector current
float V_in = 0.3;		//Voltage input source amplitude
float R_in = 500;		//Voltage input source output impedance

//Output Results
float R_E;
float R_C;
float R_B1;
float R_B2;


int main(){

	float R_out = (V_cc/2)/I_CQ;
	float R_CE_factor=0.2;
	R_E = R_out * R_CE_factor;
	R_C = R_out * (1-R_CE_factor);
	printf("R_C = %f [Ohm]\n", R_C);
	printf("R_E = %f [Ohm]\n", R_E);
	printf("V_CEQ = %f [V]\n", (V_cc+V_CE_sat)/2);
	//R_B2 = 10 * Beta * R_E
	//printf("R_B2 = ", )





}











