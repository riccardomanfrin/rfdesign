

close all;
clear all;
C=1*(10^(-12));
C_1=C; C_2=C; C_3=C;
R=2000;
Z_R1=R; 
Z_R2=R; 
Z_R3=R;
%Frquency window:
%w=(C^-1)/1.001:1:(C^-1)*1.001;
w=1:10000:1500000000;
Z_C1 = -i./(w.*C_1);
Z_C2 = -i./(w.*C_2);
Z_C3 = -i./(w.*C_3);

Z_3=Z_R3;
H_3=Z_3./(Z_3+Z_C3);

%Z_2 = Z_R2 // (Z_C3 + Z_3)
Z_2 = (Z_R1*(Z_C3+Z_3)) ./ (Z_R2+Z_C3+Z_3);
H_2 = Z_2./(Z_2+Z_C2);

%Z_1 = R // (Z_C2 + Z_2)
Z_1 = (Z_R1*(Z_C2+Z_2)) ./ (Z_R1+Z_C2+Z_2);
H_1 = Z_1./(Z_1+Z_C1);

pival=ones(1,length(w)) * pi;

phase 	= arg(H_1)+arg(H_2)+arg(H_3);
module 	= abs(H_1.*H_2.*H_3);
freq	= w./(2*pi)./1000;
resonantfreq = freq(sum(phase>=pi)-1);
attenuation	= module(sum(phase>=pi)-1);

figure(1);
hold on;
grid on;

plot(freq,pival,'g');
plot(freq,phase,'r');
plot(freq,module);
%plot(freq,20*log10(module));
legend("pi","phase","abs");
xlabel("KHz");
printf("\n");
printf("Resonant frequency: %f [KHz]\n",resonantfreq);
printf("Attenuation: %f [db] \n",20*log10(attenuation));
