f=1:10:1000;
C=1
z=20 *log10(f*C);
semilogx(f,z);
xlabel('f');
ylabel('|Z|_{dB}');
title("normalized inductor impedance")
grid on;
