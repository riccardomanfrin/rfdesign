clear all;

%Input data

Vcc=5;			% 5V
c=10^-12; 		% 1pF
r=2200;			% 2.2KOhm
Rb=7740;
Rc=750;
C1=c; C2=c; C3=c;
R1=r; R2=r; R3=r;
Reh=110;
Ceh=100*10^-6; 	% 100uF
Cout=1^10^-6; 	% 1uF
Vbe=0.65;

%Sperimental results
hfe=8.3;			% (320 from power meter) 8 was found sperimentally by measuring Vce and Ic
%hfe=32;		%Found sperimentally by measuring Vce and Ic
Voutpp=2.46;	% 2.46V
I=0.004;		% 4mA
fosc=27800;		% 27800KHz
Ve=0.4320;		% 0.432V
Vb=1.07;		%1.07V

Icsat =Vcc / (Rc+Reh)

%Calculations
%BJT bias point [all caps are open circuits]
%Vcc-Ib*Rb-Vbe-hfe*Ib*Reh=0;
%Ib=(Vcc-Vbe)/(Rb+hfe*Reh)
Ic = hfe*(Vcc-Vbe)/(Rb+hfe*Reh)
%Ic = (Vcc-Vbe)/(Reh)
Vce= Vcc - (Rc+Reh)*Ic
Vc=Ve+Vce
figure(1);
plot(Vce, Ic)