# Sample RF project

## Oscillator design

Design circuit:

![Design circuit](carrier_module.png)

Design reference https://learnabout-electronics.org/Oscillators/osc24.php

Mods:

1. Instead of bringing the feedback to the emitter the feedback is delivered to the Base.
2. The LC tank circuit values used for the project are smaller to increase the carrier frequency.

Implementation:

![Circuit implementation](scheme.jpg)

### LC inductance calculation

From https://www.allaboutcircuits.com/tools/coil-inductance-calculator/

| Turns | 8  |
|-------|----|
|Loop diameter| 4500μm |
|Wire diameter|  400μm |
|Relative permeability [μ]| 1 |
|Result | 4.52e-7H = 0.452μH|

### Carrier frequency 

Resonating frequency (carrier) is given by

    fc = 1 / ( 2 \pi \sqrt(LC))

C = 1000×220/(1000+220) = 180pF

Which gives us

	fc = 1 / (2 * 3.14 * \sqrt(180e-12 * 452e-9)) = 1 / (2 * math.pi * math.sqrt(180e-12*452e-9)) = 17644715.55740071 ~=17MHz

I think the indutances around are greater than that

# References 
1. Oscillators module 2 learnabout-electronics.org
2. https://www.youtube.com/watch?v=SnKKj2bonAI&t=676s (cutoff RL frequency, resonance LC frequency)
3. https://www.allaboutcircuits.com/tools/coil-inductance-calculator/

