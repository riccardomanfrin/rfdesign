#!/usr/bin/python

import math

L=4e-7
C=(220e-12*1e-9)/(220e-12+1e-9)
f=1/(2 * math.pi * math.sqrt(L * C))
print "{}MHz".format(f / 1e6)